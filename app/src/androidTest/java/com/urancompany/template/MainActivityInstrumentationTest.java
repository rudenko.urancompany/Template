package com.urancompany.template;

import android.test.ActivityInstrumentationTestCase2;

import com.urancompany.template.activities.MainActivity;
import com.urancompany.template.fragments.SampleFragment;

import org.junit.Before;

public class MainActivityInstrumentationTest extends ActivityInstrumentationTestCase2<MainActivity> {


    public MainActivityInstrumentationTest() {
        super(MainActivity.class);
    }


    private MainActivity mActivity;


    @Before
    public void setUp() throws Exception {
        super.setUp();
        mActivity = getActivity();
        getInstrumentation().waitForIdleSync();
    }

    public void testFragmentStarted() {
        SampleFragment fragment = (SampleFragment) mActivity.getFragmentManager().findFragmentByTag(SampleFragment.class.getSimpleName());
        assertNotNull(fragment);
        assertTrue(fragment.isVisible());

        assertNotNull(fragment.getSpiceManager());
    }



}