package com.urancompany.template.models.db.loaders;

import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;

/**
 * Created by ovi on 4/18/16.
 */
public abstract class SimpleLoaderCallbacks<T> implements LoaderManager.LoaderCallbacks<T> {

    @Override
    public void onLoaderReset(Loader<T> loader) {

    }
}
